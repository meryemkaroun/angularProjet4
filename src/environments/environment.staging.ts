export const environment = {
  production: true,
  springUrl:'https://projet4-spring-staging.herokuapp.com/',
  nodeUrl:'https://projet4-node-staging.herokuapp.com/',
};
