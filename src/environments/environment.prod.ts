export const environment = {
  production: true,
  springUrl:'https://projet4-spring-prod.herokuapp.com/',
  nodeUrl:'https://projet4-node-prod.herokuapp.com/',
};
