import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { BoardAdminComponent } from './components/board-admin/board-admin.component';
import { BoardUserComponent } from './components/board-user/board-user.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { OffersListComponent } from './components/offers-list/offers-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { SearchOffersComponent } from './components/home/search-offers/search-offers.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {NgSelectModule} from "@ng-select/ng-select";
import { OfferListComponent } from './components/home/offer-list/offer-list.component';
import { MapComponent } from './components/home/map/map.component';
import { DialogOffersComponent } from './components/home/dialog-offers/dialog-offers.component';
import {MatDialogModule} from "@angular/material/dialog";
import {NgxPaginationModule} from "ngx-pagination";
import {ChartsModule} from "ng2-charts";
import { BarChartComponent } from './components/home/bar-chart/bar-chart.component';
import { PieChartComponent } from './components/home/pie-chart/pie-chart.component';
import { IndicatorsComponent } from './components/home/indicators/indicators.component';
import {FavoriteOffersComponent} from "./components/favorite-offers/favorite-offers.component";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastrModule} from "ngx-toastr";
import { HomePageComponent } from './components/homePage/homePage.component';
import { TaskComponent } from './components/task/task.component';
import {WavesModule, ButtonsModule, IconsModule} from 'ng-uikit-pro-standard';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardUserComponent,
    OffersListComponent,
    FooterComponent,
    HeaderComponent,
    SearchOffersComponent,
    OfferListComponent,
    MapComponent,
    DialogOffersComponent,
    BarChartComponent,
    PieChartComponent,
    IndicatorsComponent,
    FavoriteOffersComponent,
    HomePageComponent,
    TaskComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    NgSelectModule,
    MatDialogModule,
    NgxPaginationModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot(),
    WavesModule,
    ButtonsModule,
    IconsModule,
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
