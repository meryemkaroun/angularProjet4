import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Offer} from "../../models/offer.model";

const nodeUrl = environment.nodeUrl + 'offres/';
const favoriteOffersUrl = environment.nodeUrl + 'favoriteOffers/';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http: HttpClient) { }

  getOffersWithCode(rome_code:string): Observable<Offer[]> {
    let params = new HttpParams().append('rome_code', rome_code);
    return this.http.get<Offer[]>(nodeUrl, {params});
  }

  getOffersWithIds(idOffersList:string[]): Observable<Offer[]> {
    let params = new HttpParams();
    params = params.append('offersIds', idOffersList.join(', '));
    return this.http.get<Offer[]>(favoriteOffersUrl, {params});
  }
}
