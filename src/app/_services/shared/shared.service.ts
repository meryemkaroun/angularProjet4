import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private favoritesOffersCount = new BehaviorSubject(0);
  sharedFavoritesOffersCount = this.favoritesOffersCount.asObservable();

  private tasksCount = new BehaviorSubject(0);
  sharedTasksCount = this.tasksCount.asObservable();

  constructor() { }

  nextCount(count: number) {
    this.favoritesOffersCount.next(count);
  }

  nextTasksCount(count: number) {
    this.tasksCount.next(count);
  }

  incrementCount() {
    this.favoritesOffersCount.next(this.favoritesOffersCount.value + 1);
  }

  incrementTasksCount() {
    this.tasksCount.next(this.tasksCount.value + 1);
  }

  decrementCount() {
    this.favoritesOffersCount.next(this.favoritesOffersCount.value - 1);
  }

  decrementTasksCount() {
    this.tasksCount.next(this.tasksCount.value - 1);
  }
}
