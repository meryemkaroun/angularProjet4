import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../../../environments/environment";
import {UserProfile} from "../../models/user-profile";

const API_URL = environment.springUrl + 'api/test/';
const API_URL_PROFILE = environment.springUrl + 'api/user/';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }

  getUserProfile(): Observable<UserProfile> {
    return this.http.get<UserProfile>(API_URL_PROFILE + 'profil');
  }

  upDateUserProfile(userProfile: UserProfile): Observable<any> {
    return this.http.post<UserProfile>(API_URL_PROFILE + 'profil', userProfile);
  }

  deleteeUserProfile(id): Observable<any> {
    return this.http.delete<UserProfile>(API_URL_PROFILE + id);
  }

}
