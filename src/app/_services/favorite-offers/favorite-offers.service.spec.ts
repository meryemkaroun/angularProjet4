import { TestBed } from '@angular/core/testing';

import { FavoriteOffersService } from './favorite-offers.service';

describe('FavoriteOffersService', () => {
  let service: FavoriteOffersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FavoriteOffersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
