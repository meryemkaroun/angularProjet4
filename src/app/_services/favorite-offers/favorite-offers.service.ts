import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {TokenStorageService} from "../token/token-storage.service";
import {Observable} from "rxjs";
import {OfferService} from "../offer/offer.service";
import {Offer} from "../../models/offer.model";
import {DomEvent} from "leaflet";
import off = DomEvent.off;

@Injectable({
  providedIn: 'root'
})
export class FavoriteOffersService {

  springUrlFav = environment.springUrl + 'api/favorites';
  constructor(private http:HttpClient,
              private tokenStorage: TokenStorageService) { }

  getFavoriteOffersCount(): Observable<number> {
    return this.http.get<number>(this.springUrlFav + '/offers/' + this.tokenStorage.getUser().id + '/count');
  }

  getFavoriteOffers(): Observable<any> {
    return this.http.get(this.springUrlFav + '/offers/' + this.tokenStorage.getUser().id);
  }

  addFavoriteOffers(offer: Offer): Observable<any> {
    return this.http.post(this.springUrlFav + '/offers/' + this.tokenStorage.getUser().id + '/' + offer._id, {});
  }

  deleteFavoriteOffers(offer: Offer): Observable<any> {
    return this.http.delete(this.springUrlFav + '/offers/' + this.tokenStorage.getUser().id + '/' + offer._id, {});
  }

  isFavoriteOffer(offer: Offer): Observable<boolean> {
    return this.http.get<boolean>(this.springUrlFav + '/offers/' + this.tokenStorage.getUser().id + '/' + offer._id + '/isFavorite');
  }

}
