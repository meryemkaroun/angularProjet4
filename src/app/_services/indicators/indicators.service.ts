import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Indicators} from "../../models/indicators";
import {CountData} from "../../models/count-data";

const nodeUrl = environment.nodeUrl + 'indicateurs/';
const offersByCityIndicatorUrl = environment.nodeUrl + 'offresParVille/';
const offersByCompanyIndicatorUrl = environment.nodeUrl + 'companiesWithMostOffers/';

@Injectable({
  providedIn: 'root'
})
export class IndicatorsService {

  constructor(private http: HttpClient) { }

  getIndicators(): Observable<Indicators> {
    return this.http.get<Indicators>(nodeUrl);
  }

  getOffersByCity(): Observable<CountData[]> {
    return this.http.get<CountData[]>(offersByCityIndicatorUrl);
  }

  getOffersByCompany(): Observable<CountData[]> {
    return this.http.get<CountData[]>(offersByCompanyIndicatorUrl);
  }

}
