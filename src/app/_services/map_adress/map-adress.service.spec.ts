import { TestBed } from '@angular/core/testing';

import { MapAdressService } from './map-adress.service';

describe('MapAdressService', () => {
  let service: MapAdressService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapAdressService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
