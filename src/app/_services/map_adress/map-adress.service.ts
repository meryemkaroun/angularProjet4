import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class MapAdressService {

  headers = new HttpHeaders({
      'Access-Control-Allow-Origin':'*',
      'Authorization':'authkey',
      'userid':'1'
    });

  constructor(private http: HttpClient) {
  }

  getCordinatesFromAddress(address: string): Observable<any> {
    const params = new HttpParams().append('q', address);
    return this.http.get<any>("https://api-adresse.data.gouv.fr/search/", {params})
      .pipe(map(result => result.features[0].geometry.coordinates));
  }
}
