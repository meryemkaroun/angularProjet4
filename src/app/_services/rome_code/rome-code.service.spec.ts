import { TestBed } from '@angular/core/testing';

import { RomeCodeService } from './rome-code.service';

describe('RomeCodeService', () => {
  let service: RomeCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RomeCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
