import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {RomeCode} from "../../models/rome-code.model";

const nodeUrl = environment.nodeUrl + 'metiers';

@Injectable({
  providedIn: 'root'
})
export class RomeCodeService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<RomeCode[]> {
    return this.http.get<RomeCode[]>(nodeUrl);
  }
}
