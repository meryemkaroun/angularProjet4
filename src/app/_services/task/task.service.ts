import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {TokenStorageService} from "../token/token-storage.service";
import {Observable} from "rxjs";
import {Offer} from "../../models/offer.model";
import {Task} from "../../models/task";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  springUrlTask = environment.springUrl + 'api/tasks';
  constructor(private http:HttpClient,
              private tokenStorage: TokenStorageService) { }

  getTasksCount(): Observable<number> {
    return this.http.get<number>(this.springUrlTask + '/' + this.tokenStorage.getUser().id + '/count');
  }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.springUrlTask + '/' + this.tokenStorage.getUser().id);
  }

  addTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.springUrlTask + '/' + this.tokenStorage.getUser().id, task);
  }

  updateTask(task: Task): Observable<any> {
    return this.http.put(this.springUrlTask + '/' + this.tokenStorage.getUser().id, task);
  }

  deleteTask(taskId: number): Observable<any> {
    return this.http.delete(this.springUrlTask + '/' + this.tokenStorage.getUser().id + '/' + taskId, {});
  }



}
