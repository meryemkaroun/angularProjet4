import {Component, Input, OnInit} from '@angular/core';
import {Offer} from "../../../models/offer.model";
import {DialogOffersComponent} from "../dialog-offers/dialog-offers.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.css']
})
export class OfferListComponent implements OnInit {

  @Input() offers?: Offer[];
  offer: Offer;
  p: number = 1;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  openDialog(offer: Offer) {
    this.dialog.open(DialogOffersComponent, {
      data: {
        offer: offer
      }
    });
  }
}
