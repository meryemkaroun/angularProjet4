import { Component, OnInit } from '@angular/core';
import {Indicators} from "../../../models/indicators";
import {IndicatorsService} from "../../../_services/indicators/indicators.service";

@Component({
  selector: 'app-indicators',
  templateUrl: './indicators.component.html',
  styleUrls: ['./indicators.component.css']
})
export class IndicatorsComponent implements OnInit {

  indicators?: Indicators;
  constructor(private indicatorsService: IndicatorsService) { }

  ngOnInit(): void {
    this.indicatorsService.getIndicators().subscribe(
      data => {
        this.indicators = data;
      },
      err => {
        this.indicators = JSON.parse(err.error).message;
      }
    );
  }

}
