import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartOptions, ChartType, ChartDataSets} from 'chart.js';
import {OfferService} from "../../../_services/offer/offer.service";
import {CountData} from "../../../models/count-data";

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnChanges {

  @Input() data?: CountData[];

  pieChartOptions: ChartOptions = {
    responsive: true,
  };

  public pieChartLabels: string[];
  @Input() pieChartType: ChartType = 'pie';
  pieChartLegend = true;
  pieChartPlugins = [];

  public pieChartData: ChartDataSets[] = [];

  constructor(public offerService: OfferService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.data) {
      this.pieChartLabels = this.data.map( data => data._id);
      this.pieChartData = [
        {data: this.data.map( data => data.count), backgroundColor: ["#77E8ED","#7EDCED","#71D0FA","#7EDCF7","#6BE8D4","#7EC5D4","#64B9FA","#64B9C8","#58A2AF","#4B8B96"]}
      ];
    }
  }
}
