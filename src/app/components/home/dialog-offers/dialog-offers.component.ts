import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Offer} from "../../../models/offer.model";
import {FavoriteOffersService} from "../../../_services/favorite-offers/favorite-offers.service";
import {SharedService} from "../../../_services/shared/shared.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-dialog-offers',
  templateUrl: './dialog-offers.component.html',
  styleUrls: ['./dialog-offers.component.css']
})
export class DialogOffersComponent implements OnInit {

  offer: Offer;
  isFavorite = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public favoriteOfferService: FavoriteOffersService,
              private toastr: ToastrService,
              private sharedService: SharedService) {
    this.offer = data.offer;
  }

  ngOnInit(): void {
    this.favoriteOfferService.isFavoriteOffer(this.offer)
      .subscribe(isFavorite => this.isFavorite = isFavorite);
  }

  addFavoriteOffers(offer: Offer) {
    console.log(offer);
    this.favoriteOfferService.addFavoriteOffers(offer).subscribe(() => {
        this.isFavorite = true;
        this.sharedService.incrementCount();
        this.toastr.success('Offre ajoutée à vos offres favorites', '', {positionClass: 'toast-top-center', timeOut: 2000});
      }
    );
  }
}
