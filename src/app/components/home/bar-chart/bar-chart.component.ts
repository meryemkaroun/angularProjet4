import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartOptions, ChartType, ChartDataSets} from 'chart.js';
import {OfferService} from "../../../_services/offer/offer.service";
import {CountData} from "../../../models/count-data";

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnChanges {

  @Input() data: CountData[];

  barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
          callback(value: string, index: number, values: number[] | string[]) {
            if (value.length > 15) {
              return value.substring(0, 15) + '...';
            }
            return value;
          },
          display: true
        },
      }],
      yAxes: [
        {
          ticks:{
            beginAtZero: true
          },
          gridLines: {
            drawOnChartArea: false
          }
        }
      ]
    }
  };

  public barChartLabels: string[];
  @Input() barChartType: ChartType = 'bar';
  barChartLegend = false;
  barChartPlugins = [];

  public barChartData: ChartDataSets[] = [];

  constructor(public offerService: OfferService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.data) {
      this.barChartLabels = this.data.map( data => data._id);
      this.barChartData = [
        {data: this.data.map( data => data.count), barThickness:25, backgroundColor: ["#5e4fa2", "#745998", "#8a638d", "#a06d83", "#b57678", "#cb806e", "#e18a63", "#EAAD91", "#EDB8A1", "#F0C4B1"]}
      ];
    }
  }
}
