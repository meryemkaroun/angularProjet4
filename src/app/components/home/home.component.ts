import {Component, OnInit} from '@angular/core';
import {UserService} from '../../_services/user/user.service';
import {OfferService} from "../../_services/offer/offer.service";
import {Offer} from "../../models/offer.model";
import {IndicatorsService} from "../../_services/indicators/indicators.service";
import {CountData} from "../../models/count-data";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  content?: string;
  offers?: Offer[];
  offersByCity?: CountData[];
  offersByCompany?: CountData[];

  constructor(private offerService: OfferService,
              private userService: UserService,
              private indicatorsService: IndicatorsService) {
  }

  ngOnInit(): void {
    this.userService.getPublicContent().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    this.indicatorsService.getOffersByCity().subscribe(
      data => {
        this.offersByCity = data;
      },
      error => {
        console.log(error);
      });

    this.indicatorsService.getOffersByCompany().subscribe(
      data => {
        data = data.slice(1, 9);
        this.offersByCompany = data;
      },
      error => {
        console.log(error);
      });
    }

  retrieveOffers(romeCode: string): void {
    this.offerService.getOffersWithCode(romeCode)
      .subscribe(
        data => {
          this.offers = data;
        },
        error => {
          console.log(error);
        });
  }

}
