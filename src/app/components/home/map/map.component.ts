import {Component, AfterViewInit, OnInit, Input, OnChanges, SimpleChanges,} from '@angular/core';
import * as L from 'leaflet';
import {Offer} from "../../../models/offer.model";
import {MapAdressService} from "../../../_services/map_adress/map-adress.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogOffersComponent} from "../dialog-offers/dialog-offers.component";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnChanges {

  @Input() offers: Offer[];
  public map: any;

  constructor(private mapAddressService: MapAdressService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.initMap();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.offers) {
      this.addOffersToMap();
    }
  }

  private initMap(): void {
    this.map = L.map('map', {
      center: [46.869326, 2.342320], zoom: 6
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 5,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

/*    const tiles = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
      attribution: 'Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.',
      id: 'mapbox.streets',
      maxZoom: 18,
      minZoom: 5,
    });*/

    tiles.addTo(this.map);
  }

  private addOffersToMap() {
    for (let offer of this.offers) {
      this.mapAddressService.getCordinatesFromAddress(offer.address).subscribe(result => {
        let circleMarker = L.circleMarker([result[1], result[0]], {
          color: 'purple',
          fillColor: '#8A2BE2',
          fillOpacity: 0.5,
          radius: 10
        }).addTo(this.map);

        circleMarker.on("click", e => {
          this.dialog.open(DialogOffersComponent, {
            data: {
              offer: offer
            }
          });

        });
      });
    }
  }
}


