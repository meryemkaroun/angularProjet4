import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {OfferService} from "../../../_services/offer/offer.service";
import {RomeCodeService} from "../../../_services/rome_code/rome-code.service";
import {Offer} from "../../../models/offer.model";
import {RomeCode} from "../../../models/rome-code.model";
import {FormControl} from "@angular/forms";
import {TokenStorageService} from "../../../_services/token/token-storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-search-offers',
  templateUrl: './search-offers.component.html',
  styleUrls: ['./search-offers.component.css']
})
export class SearchOffersComponent implements OnInit {

  @Output()
  selectRomeCode = new EventEmitter<string>();

  codesRome?: RomeCode[];
  selectedCodeRome?: RomeCode;

  myControl = new FormControl();
  roles: string[] = [];

  constructor(private romeCodeService: RomeCodeService,
              public tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.retrieveCodes();
    if (this.tokenStorageService.getToken()) {
      this.roles = this.tokenStorageService.getUser().roles;
    }
  }

  retrieveCodes(): void {
    this.romeCodeService.getAll()
      .subscribe(
        data => {
          this.codesRome = data.sort((a, b) => a.metier.localeCompare(b.metier));
        },
        error => {
          console.log(error);
        });
  }

  onClickOnSelectRomeCode(selection: any) {
    this.selectRomeCode.emit(selection.code);
  }

  isLoggedIn(): boolean {
    return this.tokenStorageService.getToken() != null;
  }
}
