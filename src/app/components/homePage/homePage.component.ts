import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbCarousel} from "@ng-bootstrap/ng-bootstrap";
import {Indicators} from "../../models/indicators";
import {IndicatorsService} from "../../_services/indicators/indicators.service";


@Component({
  selector: 'app-index',
  templateUrl: './homePage.component.html',
  styleUrls: ['./homePage.component.css']
})
export class HomePageComponent implements OnInit {

  @ViewChild('carousel', {static : true}) carousel: NgbCarousel;

  indicators?: Indicators;
  constructor(private indicatorsService: IndicatorsService) {}

  ngOnInit(): void {
    this.indicatorsService.getIndicators().subscribe(
      data => {
        this.indicators = data;
      },
      err => {
        this.indicators = JSON.parse(err.error).message;
      }
    );
  }

}
