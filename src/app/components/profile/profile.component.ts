import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../../_services/token/token-storage.service';
import {UserProfile} from "../../models/user-profile";
import {UserService} from "../../_services/user/user.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: UserProfile;
  profileFormGroup: FormGroup;

  constructor(private userService: UserService,
              private fb: FormBuilder,
              private toastr: ToastrService,
              private tokenStorageService: TokenStorageService,
              private router: Router) {}

  ngOnInit(): void {
    this.userService.getUserProfile().subscribe(user => {
      this.profileFormGroup = this.fb.group({
        id: new FormControl(user.id),
        firstName: new FormControl(user.firstName),
        lastName: new FormControl(user.lastName),
        username: new FormControl(user.username),
        email: new FormControl(user.email),
        password: new FormControl(user.password),
        tel: new FormControl(user.tel),
        gender: new FormControl(user.gender),
        address: new FormControl(user.address),
      });
      this.currentUser = user;
    });
  }

  upDateProfile(){
    this.userService.upDateUserProfile(this.profileFormGroup.value).subscribe(() =>
      this.toastr.success('Vos modifications ont bien été enregistrées', '', {positionClass: 'toast-top-center'}));
  }

  deleteUserProfile(){
    this.userService.deleteeUserProfile(this.currentUser.id).subscribe(()=>{
      this.tokenStorageService.signOut();
      this.router.navigate(['/dashboard']);
    });
  }


}
