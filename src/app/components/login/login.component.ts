import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth/auth.service';
import { TokenStorageService } from '../../_services/token/token-storage.service';
import {Router, RouterOutlet} from "@angular/router";
import {FavoriteOffersService} from "../../_services/favorite-offers/favorite-offers.service";
import {SharedService} from "../../_services/shared/shared.service";
import {TaskService} from "../../_services/task/task.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService,
              private tokenStorage: TokenStorageService,
              private router: Router,
              private favoriteOffersService: FavoriteOffersService,
              private taskService: TaskService,
              private sharedService: SharedService) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  onSubmit(): void {
    const { username, password } = this.form;

    this.authService.login(username, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.favoriteOffersService.getFavoriteOffersCount().subscribe(count => this.sharedService.nextCount(count));
        this.taskService.getTasksCount().subscribe(count => this.sharedService.nextTasksCount(count));
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

}
