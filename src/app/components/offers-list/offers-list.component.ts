import { Component, OnInit } from '@angular/core';
import {Offer} from "../../models/offer.model";
import {OfferService} from "../../_services/offer/offer.service";
import {RomeCodeService} from "../../_services/rome_code/rome-code.service";
import {RomeCode} from "../../models/rome-code.model";
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-offers-list',
  templateUrl: './offers-list.component.html',
  styleUrls: ['./offers-list.component.css']
})
export class OffersListComponent implements OnInit {

  myControl = new FormControl();
  offers?: Offer[];
  codesRome?: RomeCode[];
  currentOffer: Offer = {};
  currentIndex = -1;
  filteredOptions: any;

  constructor(private offerService: OfferService, private romeCodeService: RomeCodeService) { }

  ngOnInit(): void {
    this.retrieveCodes();
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): RomeCode[] {
    const filterValue = value.toLowerCase();

    if(this.codesRome) {
      return this.codesRome.filter(rc => rc.metier.includes(filterValue));
    } return [];
  }

  retrieveCodes(): void {
    this.romeCodeService.getAll()
      .subscribe(
        data => {
          this.codesRome = data.sort((a, b) => a.metier.localeCompare(b.metier));
        },
        error => {
          console.log(error);
        });
  }

  retrieveOffers(romeCode:string): void {
    this.offerService.getOffersWithCode(romeCode)
      .subscribe(
        data => {
          this.offers = data;
        },
        error => {
          console.log(error);
        });
  }

  setActiveOffer(offer: Offer, index: number): void {
    this.currentOffer = offer;
    this.currentIndex = index;
  }
}
