import {Component, OnInit} from '@angular/core';
import {Offer} from "../../models/offer.model";
import {FavoriteOffersService} from "../../_services/favorite-offers/favorite-offers.service";
import {OfferService} from "../../_services/offer/offer.service";
import {DialogOffersComponent} from "../home/dialog-offers/dialog-offers.component";
import {MatDialog} from "@angular/material/dialog";
import {SharedService} from "../../_services/shared/shared.service";

@Component({
  selector: 'app-favorite-offers',
  templateUrl: './favorite-offers.component.html',
  styleUrls: ['./favorite-offers.component.css']
})
export class FavoriteOffersComponent implements OnInit {

  favoriteOffers: Offer[];
  p: number = 1;

  constructor(private favoriteOffersService: FavoriteOffersService,
              private offerService: OfferService,
              public dialog: MatDialog,
              private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.initFavoritesOffers();
  }

  private initFavoritesOffers() {
    this.favoriteOffersService.getFavoriteOffers()
      .subscribe(favoritesOffersIds => {
        let offersIds = favoritesOffersIds.map(userAndId => userAndId.offerId);
        this.offerService.getOffersWithIds(offersIds).subscribe(offers => {
          this.favoriteOffers = offers;
        });
      });
  }

  openDialog(offer: Offer) {
    this.dialog.open(DialogOffersComponent, {
      data: {
        offer: offer
      }
    });
  }

  deleteFavoriteOffers(offer: Offer) {
    this.favoriteOffersService.deleteFavoriteOffers(offer).subscribe(() => {
        this.favoriteOffers.splice(this.favoriteOffers.indexOf(offer), 1);
        this.sharedService.decrementCount();
      }
    );
  }
}
