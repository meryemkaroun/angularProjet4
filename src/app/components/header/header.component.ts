import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from "../../_services/token/token-storage.service";
import {Router} from "@angular/router";
import {FavoriteOffersService} from "../../_services/favorite-offers/favorite-offers.service";
import {SharedService} from "../../_services/shared/shared.service";
import {TaskService} from "../../_services/task/task.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  roles: string[] = [];
  isCollapsed: boolean = true;
  favoriteOffersCount: number;
  tasksCount: number;

  constructor(public tokenStorageService: TokenStorageService,
              private router: Router,
              private favoriteOffersService: FavoriteOffersService,
              private sharedService: SharedService,
              private taskService: TaskService) { }

  ngOnInit(): void {
    this.sharedService.sharedFavoritesOffersCount.subscribe(count => this.favoriteOffersCount = count);
    this.sharedService.sharedTasksCount.subscribe(count => this.tasksCount = count);
    if (this.tokenStorageService.getToken()) {
      this.roles = this.tokenStorageService.getUser().roles;
      this.countFavoriteOffer();
      this.countTasks();
    }
  }

  isLoggedIn(): boolean {
    return this.tokenStorageService.getToken() != null;
  }

  logout() {
    this.tokenStorageService.signOut();
    this.router.navigateByUrl('login');
  }

  login() {
    this.tokenStorageService.signIn();
    this.router.navigateByUrl('login');
  }

  countFavoriteOffer() {
    this.favoriteOffersService.getFavoriteOffersCount().subscribe(favoriteOffersCount => {
      this.favoriteOffersCount = favoriteOffersCount;
      this.sharedService.nextCount(favoriteOffersCount);
    })
  }

  countTasks() {
    this.taskService.getTasksCount().subscribe(taskCount => {
      this.tasksCount = taskCount;
      this.sharedService.nextTasksCount(taskCount);
    })
  }
}
