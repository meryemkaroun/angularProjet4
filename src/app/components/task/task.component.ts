import {Component, OnInit} from '@angular/core';
import {Task} from "../../models/task";
import {TaskService} from "../../_services/task/task.service";
import {StatusEnum} from "../../models/status.enum";
import {SharedService} from "../../_services/shared/shared.service";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  editField: string;
  tasks: Task[];
  StatusEnum = StatusEnum;

  constructor(private taskService: TaskService,
              private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.loadTasks();
  }

  private loadTasks() {
    this.taskService.getTasks().subscribe(tasks => {
        this.tasks = tasks;
      }
    );
  }

  updateTask(id: number, property: string, event: any) {
    this.tasks[id][property] = event.target.textContent;
    let task = this.tasks[id];
    this.taskService.updateTask(task).subscribe(updated => {
      this.tasks[id] = updated;
    });
  }

  updateTaskTodo(id: number, property: string, event: any) {
    let task1 = this.tasks[id];
    task1[property] = event.target.checked ? StatusEnum.DONE : StatusEnum.TODO;
    this.taskService.updateTask(task1).subscribe(updated => {
      this.tasks[id] = updated;
    });
  }

  deleteTask(id: number) {
    const task = this.tasks[id];
    this.taskService.deleteTask(task.taskId).subscribe(() => {
        this.tasks.splice(this.tasks.indexOf(task), 1);
        this.sharedService.decrementTasksCount();
      }
    );
  }

  addTask() {
    const task = new Task();
    task.status = StatusEnum.TODO;
    task.description = 'Description par défaut';
    task.title = 'Titre par défaut';
    this.taskService.addTask(task).subscribe(task => {
      this.tasks.push(task);
      this.sharedService.incrementTasksCount();
    })
  }

}
