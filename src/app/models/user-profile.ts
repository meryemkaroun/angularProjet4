import {GenderEnum} from "./gender.enum";

export class UserProfile {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  password: string;
  tel: string;
  gender: GenderEnum;
  address: string;
}
