export class Offer {
  _id?: string;
  address?: string;
  alternance?: boolean;
  city?: string;
  contact_mode?: string;
  headcount_text?: string;
  matched_rome_code?: string;
  matched_rome_label?: string;
  name?: string;
  siret?: string;
  url?:string;
  stars?: number;
  lat?: number;
  lon?: number
}
