import {StatusEnum} from "./status.enum";

export class Task {
  taskId?: number;
  title?: string;
  description?: string;
  status?: StatusEnum
}
