export interface Indicators {
  numberOfOffers: number,
  numberOfCompanies: number,
  numberOfCities: number,
  numberOfJobs: number
}
